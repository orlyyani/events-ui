import Axios from 'axios'

class EventApi {

	constructor() {
		this.options = {
			Api: {
				Domain :'http://127.0.0.1:8000/api/'
			}
		}
		this._headers();
	}

	_headers() {
		Axios.defaults.headers.put['Content-Type'] = 'application/json';
		Axios.defaults.headers.post['Content-Type'] = 'application/json';
		Axios.defaults.baseURL = this._apiURL(); 
	}

	_apiURL() {
		return this.options.Api.Domain;
  }
  
  //Endpoints

	getAllEvents(){
		return Axios.get('events');
	}

	postEvent(data){
		return Axios.post('event', data);
	}
}

export default new EventApi();