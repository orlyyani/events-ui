import Vuex from 'vuex'
import Api from '@/api/index';
import Vue from 'vue';

Vue.use(Vuex)

export default new Vuex.Store ({
	state: {
    allEvents: []
	},
	mutations: {
    setAllEvents(state, payload){
      state.allEvents = payload
    },
    addEvent(state, payload){
      state.allEvents.push(payload)
    }
  },
	actions: {
    initializeAllEvents({commit}){
      return Api.getAllEvents().then((response) => {
        console.log(response.data)
        this.commit('setAllEvents', response.data)
      }, error => {
        console.log(error)
      });
    },

    postEvent({commit}, payload){
      return Api.postEvent(payload).then((response) => {
        console.log(response.data)
        this.commit('addEvent', response.data)
      }, error => {
        console.log(error)
      });
    }
  }
})