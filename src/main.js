// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Store from '@/store'
import iView from 'iview'
import FullCalendar from 'vue-full-calendar'
import 'iview/dist/styles/iview.css';
import { locale } from 'iview';
import lang from 'iview/dist/locale/en-US';

Vue.config.productionTip = false
Vue.use(iView)
Vue.use(FullCalendar)
locale(lang);
Vue.use(iView, { locale });

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: Store,
  components: { App },
  template: '<App/>'
})
